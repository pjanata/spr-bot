import discord
from discord.errors import HTTPException
from pathlib import Path
import zipfile
import time
import docker
import utils

TOKEN = '*******' # Put the bot's token here

discord = discord.Client()
docker_client = docker.from_env()

EVAL_IMG = 'assigment_eval'


assigments = {}


print('Starting up...')

@discord.event
async def on_message(message):
    print(message.content)
    print(message.author)
    if message.author == discord.user:
        return

    if message.attachments:
        for attachment in message.attachments:
            print(attachment.filename)
            if 'fizzbuzz' in attachment.filename:
                await eval_asssigment('fizzbuzz', 'FizzbuzzTest', message, attachment)
            elif 'ascii_house' in attachment.filename:
                await eval_asssigment('ascii_house', 'AsciiHouseTest', message, attachment)
            elif 'tic_tac_toe_player' in attachment.filename:
                await eval_asssigment('tic_tac_toe_player', 'TicTacToePlayerTest', message, attachment)
            elif 'tic_tac_toe' in attachment.filename:
                await eval_asssigment('tic_tac_toe', 'TicTacToeTest', message, attachment)
            elif 'password_check' in attachment.filename:
                await eval_asssigment('password_check', 'PasswordCheckTest', message, attachment)


    if message.content.startswith('!hello'):
        msg = 'Hello {0.author.mention}'.format(message)
        await message.channel.send(msg)

    if message.content.startswith('!yo'):
        async with message.channel.typing():
            await message.channel.send('\'sup?')


async def eval_asssigment(name: str, test_name: str, message, attachment):
    await message.channel.send(f'Found {name} assigment. Downloading it now...')

    assigment_dir = Path(str(message.author) + f'/{name}/')
    assigment_dir.mkdir(parents=True, exist_ok=True)
    assigment_path = assigment_dir / attachment.filename

    async with message.channel.typing():
        await attachment.save(assigment_dir / attachment.filename)

    await message.channel.send('File downloaded')

    if assigment_path.suffix == '.zip':
        unzip_file(assigment_path)
        await message.channel.send('Extracted')

    if not (assigment_dir / f'{name}.py').exists():
        await message.channel.send(f'No file {name}.py found')
        return

    await message.channel.send('Evaluating...')
    async with message.channel.typing():
        eval_cont = docker_client.containers.create(EVAL_IMG)
        eval_cont.start()

        time.sleep(0.5)

        utils.copy_to_container(eval_cont, '/opt/assigment_eval/tested', assigment_dir)

        exit_code, output = eval_cont.exec_run(
            f'python -m unittest -f -q assigment_eval.assigments.test_{name}.{test_name}', )

    output = str(output).replace('\\n', '\n')

    if exit_code == 0:
        await message.channel.send(':fireworks: :ok_hand: :ok_hand: :fireworks:\n :clap: :clap: :clap: :clap:')
    else:
        await message.channel.send(
            f':bangbang::x::x::bangbang:\n'
            f'Here\'s the output:\n'
        )
        try:
            await message.channel.send(
                f'\n```'
                f'{output[2:-1]}\n'
                f'```\n'
            )
        except HTTPException:
            await message.channel.send(':scream: Output is too long. Sorry about that :weary: ')

    if name == 'tic_tac_toe_player' and exit_code == 0:
        await message.channel.send('Evaluating the player against reference players')
        await eval_player(message, assigment_dir)

    print(exit_code)
    print(output)


async def eval_player(message, assigment_dir):
    async with message.channel.typing():
        eval_cont = docker_client.containers.create(EVAL_IMG)
    eval_cont.start()

    time.sleep(0.5)

    utils.copy_to_container(eval_cont, '/opt/assigment_eval/tested', assigment_dir)

    exit_code, output = eval_cont.exec_run(
        f'python -m assigment_eval.assigments.eval_player.py')
    output = str(output).replace('\\n', '\n').replace('\\t', '\t')
    if exit_code != 0:
        await message.channel.send(
            f'Player failed during the evaluation'
            f':bangbang::x::x::bangbang:\n'
            f'Here\'s the output:\n'
        )
        try:
            await message.channel.send(
                f'\n```'
                f'{output[2:-1]}\n'
                f'```\n'
            )
        except HTTPException:
            await message.channel.send(':scream: Output is too long. Sorry about that :weary: ')

    await message.channel.send(
                f'\n```'
                f'{output[2:-1]}\n'
                f'```\n'
            )



def unzip_file(path: Path):
    with zipfile.ZipFile(path, 'r') as zip_ref:
        zip_ref.extractall(path.parent)
    path.unlink()


@discord.event
async def on_ready():
    print('Logged in as')
    print(discord.user.name)
    print(discord.user.id)
    print('------')

discord.run(TOKEN)
