from io import BytesIO
from pathlib import Path
import tarfile
import time


def copy_to_container(container, path_in_container, filepath):
    with create_archive(filepath) as archive:
        container.put_archive(path_in_container, data=archive)

def create_archive(path: Path):
    pw_tarstream = BytesIO()
    pw_tar = tarfile.TarFile(fileobj=pw_tarstream, mode='w')
    for file in path.iterdir():
        pw_tar.add(file, arcname=file.name, recursive=True)
    pw_tar.close()
    pw_tarstream.seek(0)
    return pw_tarstream

if __name__ == '__main__':
    tarstrem = create_archive(Path('assigment_eval/assigments'))
    with open('bla.tar', 'wb') as f:
        f.write(tarstrem.read())

