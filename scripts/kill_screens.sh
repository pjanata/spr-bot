#!/usr/bin/env bash

id=$(screen -ls | grep Detached | cut -d. -f1 | awk '{print $1}')
[[ -z "$id" ]] && echo "No detached screen" || kill $id