from typing import List, Tuple

symbols = {0: ' ', 1: 'X', 2: 'O'}


def board_str(board: List[List[int]]) -> str:
    """
    Returns a printable string representation of the board.
    The implementation details are up to you, but it should be pretty

    :param board: 2d list representing the board
    :return: string representation of the game board
    """
    # TODO - remove the line bellow and implement this function
    raise NotImplementedError


def mark_board(board: List[List[int]], pos: Tuple[int, int], player: int) -> List[List[int]]:
    """
    Marks the game board with the player's number at a given positions.

    example:
    board = [[0, 0, 0],
            [0, 0, 0],
            [0, 0, 0]]
    pos = (2, 0)
    player = 2

    should return:
        [[0, 0, 0],
        [0, 0, 0],
        [2, 0, 0]]

    In this function you don't need to worry about the validity of the parameters.
    It's guaranteed that it will never be called with the position outside of the game board.

    :param board: 2d list representing the board
    :param pos: position which the player want's to mark
    :param player: the player number
    :return: game board with the marked position
    """
    # TODO - remove the line bellow and implement this function
    raise NotImplementedError


def get_winner(board: List[List[int]]) -> int:
    """
    Returns the winner of the game. Don't forget to check the diagonals!
    If there are no moves (every position is either 1 or 2) it's a tie
    and this function should return -1. Be careful, that there might be
    a situation where all positions are marked but some player still wins

    examples:
    [[1, 1, 2]
    [2, 1, 1],  -> should return 1
    [2, 2, 1]]

    [[1, 1, 2]
    [2, 2, 1],  -> should return -1, it's a tie
    [1, 2, 1]]


    :param board: 3x3 list of integers representing game board.
    :return: 0 if there is no winner, 1 if Player 1 won, 2 if Player 2 won
             and -1 if it's a tie - there are no more moves
    """
    # TODO - remove the line bellow and implement this function
    raise NotImplementedError


def get_input(board: List[List[int]], player: int) -> Tuple[int, int]:
    """
    gets the user input and makes sure it's valid
    """
    while True:
        try:
            x, y = (int(x) for x in input(f'Player {player}\'s ({symbols[player]}) turn:\n').split())
            if not 0 <= x < 3 or not 0 <= y < 3:
                print('Value must be in range from 0 to 3')
            elif board[x][y] != 0:
                print('This position is already marked')
            else:
                return x, y
        except ValueError:
            print("Invalid value, input should be two numbers separated by space")
        except:
            print("Invalid input")



def change_player(player: int) -> int:
    return 1 if player == 2 else 2


def run():
    """
    Main game loop, if you implement all the function correctly,
    calling this function should run the game
    """
    board = [[0] * 3 for _ in range(3)]  # empty board
    player = 1
    winner = 0

    print(board_str(board))

    while winner == 0:
        pos = get_input(board, player)

        board = mark_board(board, pos, player)

        print(board_str(board))

        winner = get_winner(board)
        player = change_player(player)

    if winner == -1:
        print('It\'s a tie!')
    elif winner == 1:
        print(f'Player 1 with {symbols[1]} won!')
    else:
        print(f'Player 2 with {symbols[2]} won!')

    return winner


if __name__ == '__main__':
    run()