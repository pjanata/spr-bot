install:
	pip install -r requirements.txt
	docker build assigment_eval -t assigment_eval

run:
	python bot.py

run-backgroud:
	screen -d -m python bot.py

kill-backgroud:
	scripts/kill_screens.sh

update:
	git fetch;
	git pull

auto-update: kill-backgroud update install run-backgroud

