import pandas as pd
from pathlib import Path
import importlib.util
from assigment.tic_tac_toe.game import TicTacToe
from typing import Tuple
from collections import namedtuple
from itertools import product
from progressbar import progressbar
from copy import deepcopy
from assigment_eval.reference.tic_tac_toe_player import MiniMaxPlayer


pd.set_option('display.max_columns', 20)

Result = namedtuple('Result', ['wins', 'ties', 'losses'])

script_dir = Path(__file__).parent

def get_players(path: Path):
    players = dict()

    for submission in path.iterdir():
        name = submission.name[:submission.name.find('_')]
        module_name = '_'.join(name.lower().split())
        spec = importlib.util.spec_from_file_location(module_name, submission / 'tic_tac_toe_player.py')
        player = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(player)
        players[name] = player.Player

    return players

def get_reference_players(prob = 0.3):
    return MiniMaxPlayer(1, prob), MiniMaxPlayer(2, prob)



def run_match(player1, player2, num_games=100) -> Tuple[int, int, int]:

    res = [0, 0, 0]
    print(f'{player1} {player2}')

    for _ in progressbar(range(num_games)):
        game = TicTacToe(deepcopy(player1), deepcopy(player2))
        outcome = game.run()
        if outcome == 1:
            res[0] += 1
        elif outcome == -1:
            res[1] += 1
        else:
            res[2] += 1
    return Result(*res)

def eval_all() -> pd.DataFrame:
    ref_players = get_reference_players()
    players = get_players(script_dir / 'assigments')

    results = {}
    n = 1000
    for name, player in progressbar(players.items()):

        _, _, loss1 = run_match(player(1), ref_players[1], num_games=n//2)
        loss2, _, _ = run_match(ref_players[0], player(2), num_games=n//2)
        results[name] = n - loss1 - loss2

    res_series = pd.Series(results)
    res = pd.DataFrame([res_series, res_series >= n //2]).transpose()
    return res


if __name__ == '__main__':
    print(eval_all())

