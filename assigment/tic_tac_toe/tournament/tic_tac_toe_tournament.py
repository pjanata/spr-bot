import pandas as pd
from pathlib import Path
import importlib.util
from assigment.tic_tac_toe.game import TicTacToe
from typing import Tuple
from collections import namedtuple
from itertools import product
import pickle
from lxml.html import tostring, fromstring
from lxml.html import builder as E
from progressbar import progressbar
from copy import deepcopy

pd.set_option('display.max_columns', 20)

Result = namedtuple('Result', ['wins', 'ties', 'losses'])

script_dir = Path(__file__).parent

def get_players(path: Path):
    players = dict()

    for submission in path.iterdir():
        name = submission.name[:submission.name.find('_')]
        module_name = '_'.join(name.lower().split())
        spec = importlib.util.spec_from_file_location(module_name, submission / 'tic_tac_toe_player.py')
        player = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(player)
        players[name] = player.Player

    return players

def run_match(player1, player2, num_games=100) -> Tuple[int, int, int]:
    res = [0, 0, 0]
    print(f'{player1} {player2}')
    p1 = player1(1)
    p2 = player2(2)
    for _ in progressbar(range(num_games)):
        game = TicTacToe(deepcopy(p1), deepcopy(p2))
        outcome = game.run()
        if outcome == 1:
            res[0] += 1
        elif outcome == -1:
            res[1] += 1
        else:
            res[2] += 1
    return Result(*res)

def round_robin() -> pd.DataFrame:
    players = get_players(script_dir / 'assigments')

    players_names = players.keys()
    matches = list(product(players_names, players_names))
    player_classes = [(players[p1], players[p2]) for p1, p2 in matches]

    df = pd.DataFrame(index=players.keys(), columns=players.keys(), dtype=object)

    results = []
    for player_pair in progressbar(player_classes):
        results.append(run_match(*player_pair))

    for (p1, p2), res in zip(matches, results):
        df[p2][p1] = res

    return df

def get_score(result_df: pd.DataFrame):
    players = result_df.index
    score_df = result_df.applymap(lambda res: res[0] - res[2])

    scores = {}

    for player in players:
        scores[player] = sum(score_df.loc[player]) - sum(score_df[player]) # + abs(score_df[player][player])

    return pd.Series(scores).sort_values(ascending=False)

def save_to_html(result_df: pd.DataFrame, outfile: Path):
    html = E.HTML(
        E.HEAD(
            E.TITLE('Tic Tac Toe Results')
        ),
        E.BODY(
                E.H2('(wins, ties, losses)'),
                fromstring(result_df.to_html()),
                E.BR,
                fromstring(pd.DataFrame(get_score(result_df)).to_html())
        )
    )
    with outfile.open('wb') as f:
        f.write(tostring(html))


if __name__ == '__main__':
    res_df = round_robin()
    with open('res_df.pkl', 'wb') as f:
        pickle.dump(res_df, f)

    save_to_html(res_df, script_dir / 'res.html')



