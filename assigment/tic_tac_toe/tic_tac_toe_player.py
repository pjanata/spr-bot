from typing import List, Generator, Tuple, Dict
from copy import deepcopy
import random

symbols = {0: ' ', 1: 'X', 2: 'O'}


class BasePlayer:
    def __init__(self, player_num: int):
        self.player_num = player_num

    def play_move(self, board: List[List[int]]) -> Tuple[int, int]:
        raise NotImplementedError


class HumanPlayer(BasePlayer):
    def __init__(self, player_num: int):
        super().__init__(player_num)

    def play_move(self, board: List[List[int]]) -> Tuple[int, int]:
        """
        gets the user input and makes sure it's valid
        """
        while True:
            try:
                x, y = (int(x) for x in input(f'Player {self.player_num}\'s ({symbols[self.player_num]}) turn:\n').split())
                if not 0 <= x < 3 or not 0 <= y < 3:
                    print('Value must be in range from 0 to 3')
                elif board[x][y] != 0:
                    print('This position is already marked')
                else:
                    return x, y
            except ValueError:
                print("Invalid value, input should be two numbers separated by space")
            except:
                print("Invalid input")


class MyPlayer(BasePlayer):
    def __init__(self, player_num: int):
        super().__init__(player_num)
        # Doplnte vasi inicializaci

    def play_move(self, board: List[List[int]]) -> Tuple[int, int]:
        """
        Returns a valid move

        :param board: game board represented as a 2D list
        :return: tuple of two values denoting a desired move - (row position, column position).
        Value (0, 0) corresponds to a top left corner.
        """
        # Doplnte vas kod
        raise NotImplementedError


Player = MyPlayer
