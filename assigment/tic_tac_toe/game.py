from typing import List, Tuple
from assigment.tic_tac_toe.tic_tac_toe_player import BasePlayer, HumanPlayer, Player
from copy import deepcopy
import time

symbols = {0: ' ', 1: 'X', 2: 'O'}

class TicTacToe():
    def __init__(self, player1: BasePlayer, player2: BasePlayer):
        self.player1 = player1
        self.player2 = player2

        self.player = self.player1

        self.board = [[0, 0, 0],
                      [0, 0, 0],
                      [0, 0, 0]]


    def run(self):
        """
        Main game loop, if you implement all the function correctly,
        calling this function should run the game
        """
        winner = 0

        # print(self.board_str())

        while winner == 0:
            t1 = time.time()
            pos = self.player.play_move(deepcopy(self.board))
            # print(f'Took {time.time()-t1}s')

            if self.board[pos[0]][pos[1]] != 0:
                self.change_player()
                # print(f'Invalid move, Player {self.player.player_num} wins')
                return self.player.player_num

            self.board = self.mark_board(pos)

            # print(self.board_str())

            winner = self.get_winner()
            self.change_player()

        # if winner == -1:
        #     print('It\'s a tie!')
        # elif winner == 1:
        #     print(f'Player 1 with {symbols[1]} won!')
        # else:
        #     print(f'Player 2 with {symbols[2]} won!')

        return winner


    def board_str(self) -> str:
        """
        Return printable string representation of the board.
        The implementation details are up to you, but it should be pretty

        :return: string representation of the game board
        """
        header = f'   {"   ".join(str(x) for x in range(3))}\n'
        horizontal_line = f' {"―" * (3 * 4 + 1)}\n'
        board_rows = [f'{idx}| {" | ".join(symbols[x] for x in line)} |\n' for idx, line in enumerate(self.board)]

        out_str = header

        out_str += horizontal_line.join(board_rows)
        return out_str

    def mark_board(self, pos: Tuple[int, int]) -> List[List[int]]:
        """
        Marks the game board with the player's number at a given positions.

        example:
        board = [[0, 0, 0],
                [0, 0, 0],
                [0, 0, 0]]
        pos = (2, 0)
        player = 2

        should return:
            [[0, 0, 0],
            [0, 0, 0],
            [2, 0, 0]]

        In this function you don't need to worry about the validity of the parameters.
        It's guaranteed that it will never be called with the position outside of the game board.

        :param board: 2d list representing the board
        :param pos: position which the player want's to mark
        :param player: the player number
        :return: game board with the marked position
        """
        x, y = pos
        self.board[x][y] = self.player.player_num
        return self.board

    def get_winner(self) -> int:
        """
        Returns the winner of the game. Don't forget to check the diagonals!
        If there are no moves (every position is either 1 or 2) it's a tie
        and this function should return -1. Be careful, that there might be
        a situation where all positions are marked but some player still wins

        examples:
        [[1, 1, 2]
        [2, 1, 1],  -> should return 1
        [2, 2, 1]]

        [[1, 1, 2]
        [2, 2, 1],  -> should return -1, it's a tie
        [1, 2, 1]]

        :return: 0 if there is no winner, 1 if Player 1 won, 2 if Player 2 won
                 and -1 if it's a tie - there are no more moves
        """
        for row in self.board:
            same = self.check_row(row)
            if same != 0:
                return same
        for col in map(list, zip(*self.board)):
            same = self.check_row(col)
            if same != 0:
                return same

        diagonals = self.check_diags()
        if diagonals != 0:
            return diagonals

        return -1 if self.is_tie() else 0

    def check_diags(self) -> int:
        diag_1 = self.check_row([self.board[x][x] for x in range(3)])
        return diag_1 if diag_1 != 0 else self.check_row([self.board[x][2-x] for x in range(3)])

    def check_row(self, row: List[int]) -> int:
        first = row[0]
        if all(el == first for el in row):
            return first
        return 0

    def is_tie(self) -> bool:
        for row in self.board:
            for col in row:
                if col == 0:
                    return False
        return True

    def change_player(self):
        self.player = self.player1 if self.player is self.player2 else self.player2

if __name__ == '__main__':
    # Player against Human
    p1 = Player(1)
    p2 = HumanPlayer(2)

    # Player against Human, swapped order
    # p1 = HumanPlayer(1)
    # p2 = Player(2)

    # Player against itself
    # p1 = Player(1)
    # p2 = Player(2)

    TicTacToe(p1, p2).run()


