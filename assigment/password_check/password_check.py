special_chars = {'!', '#', '$', '%', '&', '@', '.', '?', '_'}


def check_password(password: str) -> bool:
    if not is_long_enough(password):
        print('Password should be at least 12 characters long')
        return False
    elif not has_uppercase(password):
        print('Password should include at least one uppercase letter')
        return False
    elif not has_lowercase(password):
        print('Password should include at least one lowercase letter')
        return False
    elif not has_digit(password):
        print('Password should include at least one digit')
        return False
    elif not has_special_char(password):
        print(f'Password should include at least one special character (\'&!._@$?#%\')')
        return False

    return True


def is_long_enough(password: str) -> bool:
    """
    True if password is at least 12 characters long
    """
    pass


def has_uppercase(password: str) -> bool:
    """
    True if at least one letter is uppercase
    """
    pass


def has_lowercase(password: str) -> bool:
    """
    True if at least one letter is lowercase
    """
    pass


def has_digit(password: str) -> bool:
    """
    True if password contains at least one digit
    """
    pass


def has_special_char(password: str) -> bool:
    """
    True if password contains at least one special character (from '&!._@$?#%')
    """
    pass

