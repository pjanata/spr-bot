## Assignment validation discord bot

This simple discord bot served as a simple homework assignment validation server. 

It reads messages to send to it, and when there is an attachment (either .zip or .py), it will try to evaluate it.

This means the bot will execute arbitrary python code send to it! 
To somewhat mitigate the risks, it executes the evaluation in a docker container. That way, it should at least not have access to the file system and network.

After the evaluation is finished, it attempts to convey the result to the original sender. 
