from contextlib import contextmanager
import signal
from typing import List

class TimeoutException(Exception): pass

class PlayerException(Exception):
    def __init__(self, player, board):
        self.player = player
        self.board = board


class PlayerTimeOutException(PlayerException):
    pass

    def __str__(self):
        return f'\nPlayer {self.player.player_num} with {"X" if self.player.player_num == 1 else "O"}, timed out on a following ' \
               f'board:\n{board_str(self.board)}'


class InvalidMoveException(PlayerException):
    def __init__(self, player, board, move):
        super().__init__(player, board)
        self.move = move

    def __str__(self):
        return f'\nPlayer {self.player.player_num} with {"X" if self.player.player_num == 1 else "O"}, played an invalid move' \
               f'at {self.move} on a flowing board:\n{board_str(self.board)}'


@contextmanager
def time_limit(seconds):
    def raise_exp(_, __):
        raise TimeoutException()

    signal.signal(signal.SIGALRM, raise_exp)
    signal.alarm(seconds)

    try:
        yield
    except TimeoutError:
        pass
    finally:
        signal.signal(signal.SIGALRM, signal.SIG_IGN)

symbols = {0: ' ', 1: 'X', 2: 'O'}

def board_str(board: List[List[int]]) -> str:
    """
    Return printable string representation of the board.
    The implementation details are up to you, but it should be pretty

    :param board: 2d list representing the board
    :return: string representation of the game board
    """
    header = f'   {"   ".join(str(x) for x in range(3))}\n'
    horizontal_line = f' {"=" * (3 * 4 + 1)}\n'
    board_rows = [f'{idx}| {" | ".join(symbols[x] for x in line)} |\n' for idx, line in enumerate(board)]

    out_str = header

    out_str += horizontal_line.join(board_rows)
    return out_str