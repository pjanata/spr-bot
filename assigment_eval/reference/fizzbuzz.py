def fizzbuzz(n):
    for i in range(1, n+1):
        out_str = ""

        if i % 3 == 0:
            out_str += "fizz"
        if i % 5 == 0:
            out_str += "buzz"

        print(out_str) if out_str else print(i)

if __name__ == '__main__':
    fizzbuzz(100)
