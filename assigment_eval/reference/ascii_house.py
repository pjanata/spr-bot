X = 'X'


def draw_house(width: int, height: int) -> None:
    if (width % 2 == 0) or width < 3 or height < 3:
        raise ValueError("Invalid argument")
    _draw_roof(width)
    _draw_base(width, height)


def _draw_roof(width: int) -> None:
    num_spaces = (width // 2)
    first_line = " " * num_spaces + X
    print(first_line)
    for row in range(1, (width // 2)):
        start_spaces = (num_spaces - row)
        middle_spaces = (2*row-1)

        line = start_spaces * " " + X + middle_spaces * " " + X

        print(line)


def _draw_base(width: int, height: int) -> None:
    _draw_full_row(width)

    wall = X + (width-2) * " " + X
    for _ in range(height-2):
        print(wall)

    _draw_full_row(width)


def _draw_full_row(width: int) -> None:
    print(width * X)


if __name__ == '__main__':
    draw_house(11, 4)
