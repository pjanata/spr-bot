from typing import List, Generator, Tuple, Dict
from copy import deepcopy
import random
import pickle
from pathlib import Path

symbols = {0: ' ', 1: 'X', 2: 'O'}
script_dir = Path(__file__).parent


class BasePlayer:
    def __init__(self, player_num: int):
        self.player_num = player_num

    def play_move(self, board: List[List[int]]) -> Tuple[int, int]:
        raise NotImplementedError


class HumanPlayer(BasePlayer):
    def __init__(self, player_num: int):
        super().__init__(player_num)

    def play_move(self, board: List[List[int]]) -> Tuple[int, int]:
        """
        gets the user input and makes sure it's valid
        """
        while True:
            try:
                x, y = (int(x) for x in input(f'Player {self.player_num}\'s ({symbols[self.player_num]}) turn:\n').split())
                if not 0 <= x < 3 or not 0 <= y < 3:
                    print('Value must be in range from 0 to 3')
                elif board[x][y] != 0:
                    print('This position is already marked')
                else:
                    return x, y
            except ValueError:
                print("Invalid value, input should be two numbers separated by space")
            except:
                print("Invalid input")


class MiniMaxPlayer(BasePlayer):
    def __init__(self, player_num: int, difficulty: float = 1):
        super().__init__(player_num)

        self.p_best = difficulty
        self.first_move = True

        self.board_values: Dict[Tuple[int, List[List[int]]], int] = {}
        self.gen_values()

    def gen_values(self):
        if self.player_num == 1:
            board = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
            for pos in self.get_moves(board):
                succ = self.successor(board, pos, self.player_num)
                self.get_board_value(succ, self.player_num)
        else:
            boards = (
                [[1, 0, 0], [0, 0, 0], [0, 0, 0]],
                [[0, 0, 0], [0, 1, 0], [0, 0, 0]],
                [[0, 1, 0], [0, 0, 0], [0, 0, 0]]
            )
            for board in boards:
                for pos in self.get_moves(board):
                    succ = self.successor(board, pos, self.player_num)
                    self.get_board_value(succ, self.player_num)

    def play_move(self, board: List[List[int]]):

        value = -1000
        best_pos = None
        for pos in self.get_moves(board):
            succ = self.successor(board, pos, self.player_num)
            succ_value = self.get_board_value(succ, self.player_num)
            if succ_value > value:
                value = succ_value
                best_pos = pos
        #print(f'Player {self.player_num} plays\n{best_pos[0]} {best_pos[1]}')
        return best_pos if random.random() <= self.p_best else random.choice(list(self.get_moves(board)))


    def minimax(self, board: List[List[int]], player: int):
        winner = self.get_winner(board)
        if winner != 0:
            return self.utility_from_winner(winner)

        if player == self.player_num:
            value = -1000
            for pos in self.get_moves(board):
                succ = self.successor(board, pos, player)
                succ_value = self.get_board_value(succ, player)
                value = max(value, succ_value)
            return value
        else:
            value = 1000
            for pos in self.get_moves(board):
                succ = self.successor(board, pos, player)
                succ_value = self.get_board_value(succ, player)
                value = min(value, succ_value)
            return value


    def get_board_value(self, board: List[List[int]], player) -> int:
        board_tup = tuple(tuple(row) for row in board)
        for board_sym in self.board_symmetries(board_tup):
            if board_sym in self.board_values:
                return self.board_values[board_sym]

        value = self.minimax(board, self.switch_player(player))
        self.board_values[board_tup] = value
        return value

    def board_symmetries(self, board: tuple) -> Generator[tuple, None, None]:
        yield board
        board_r = self.rotate(board)
        yield board_r
        board_r = self.rotate(board_r)
        yield board_r
        yield tuple(zip(*board_r))
        board_r = self.rotate(board_r)
        yield board_r


        yield board[::-1]

        left_right = tuple(row[::-1] for row in board)
        yield tuple(row[::-1] for row in board)

        yield tuple(zip(*board))
        yield tuple(zip(*left_right))



    def rotate(self, board: tuple) -> tuple:
        return tuple(zip(*board[::-1]))

    def switch_player(self, player_num: int):
        return 1 if player_num == 2 else 2


    def utility_from_winner(self, winner: int) -> int:
        if winner == -1:
            return 0
        elif winner == self.player_num:
            return 1
        else:
            return -1

    @staticmethod
    def get_moves(board: List[List[int]]) -> Generator[Tuple[int, int], None, None]:
        for x, row in enumerate(board):
            for y, value in enumerate(row):
                if value == 0:
                    yield x, y

    @staticmethod
    def successor(board: List[List[int]], move: Tuple[int, int], player_num: int) -> List[List[int]]:
        board_cp = deepcopy(board)
        x, y = move
        board_cp[x][y] = player_num
        return board_cp

    def get_winner(self, board: List[List[int]]) -> int:
        for row in board:
            same = self.check_row(row)
            if same != 0:
                return same
        for col in map(list, zip(*board)):
            same = self.check_row(col)
            if same != 0:
                return same

        diagonals = self.check_diags(board)
        if diagonals != 0:
            return diagonals

        return -1 if self.is_tie(board) else 0

    @staticmethod
    def check_diags(board: List[List[int]]) -> int:
        diag_1 = MiniMaxPlayer.check_row([board[x][x] for x in range(3)])
        return diag_1 if diag_1 != 0 else MiniMaxPlayer.check_row([board[x][2 - x] for x in range(3)])

    @staticmethod
    def check_row(row: List[int]) -> int:
        first = row[0]
        if all(el == first for el in row):
            return first
        return 0

    @staticmethod
    def is_tie(board: List[List[int]]) -> bool:
        for row in board:
            for col in row:
                if col == 0:
                    return False
        return True

    @staticmethod
    def save_players():
        players = {'random': (MiniMaxPlayer(1, 0), MiniMaxPlayer(2, 0)),
                   'medium': (MiniMaxPlayer(1, 0.5), MiniMaxPlayer(2, 0.5)),
                   'hard': (MiniMaxPlayer(1, 1), MiniMaxPlayer(2, 1))}
        with (script_dir / 'players.pkl').open('wb') as f:
            pickle.dump(players, f)

    @staticmethod
    def load_players() -> Dict[str, Tuple['MiniMaxPlayer', 'MiniMaxPlayer']]:
        with (script_dir / 'players.pkl').open('rb') as f:
            return pickle.load(f)





if __name__ == '__main__':
    board = [[1, 2, 0],
             [1, 0, 0],
             [0, 0, 0]]
    player1 = MiniMaxPlayer(1)
    MiniMaxPlayer.save_players()
    players = MiniMaxPlayer.load_players()


