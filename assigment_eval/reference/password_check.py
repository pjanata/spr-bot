special_chars = {'!', '#', '$', '%', '&', '@', '.', '?', '_'}


def check_password(password: str) -> bool:
    if not is_long_enough(password):
        print('Password should be at least 12 characters long')
        return False
    elif not has_uppercase(password):
        print('Password should include at least one uppercase letter')
        return False
    elif not has_lowercase(password):
        print('Password should include at least one lowercase letter')
        return False
    elif not has_digit(password):
        print('Password should include at least one digit')
        return False
    elif not has_special_char(password):
        print(f'Password should include at least one special character (\'&!._@$?#%\')')
        return False

    return True


def is_long_enough(password: str):
    """
    True if password is at least 12 characters long
    """
    return len(password) >= 12


def has_uppercase(password: str):
    """
    True if at least one character is uppercase
    """
    return any(ch.isupper() for ch in password)


def has_lowercase(password: str):
    """
    True if at least one character is lowercase
    """
    return any(ch.islower() for ch in password)


def has_digit(password: str):
    """
    True if password contains at least one digit
    """
    return any(ch.isdigit() for ch in password)


def has_special_char(password: str):
    """
    True if password contains at least one special character (from '&!._@$?#%')
    """
    return any(ch in special_chars for ch in password)


print(check_password('nasjKJDcas!@#34r23'))
print(check_password('nasjKJDcas!@#r'))
print(check_password('nasjcas!@#34r23'))
print(check_password('nasjKJDcas34r23'))
print(check_password('njKJD!#23'))