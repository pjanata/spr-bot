import assigment_eval.tested.tic_tac_toe as tic_tac_toe
from assigment_eval.assigments import captured_output
from copy import deepcopy

import unittest


class TicTacToeTest(unittest.TestCase):

    def test_board_str_implemented(self):
        board = [[0, 1, 0],
                 [2, 2, 1],
                 [1, 2, 0]]

        try:
            tic_tac_toe.board_str(board)
        except NotImplementedError:
            self.fail('The board_str() function is either not implemented, or you forgot to delete the '
                      '\"raise NotImplemented\" expresion')


    def test_board_str(self):
        board = [[0, 1, 0],
                 [2, 2, 1],
                 [1, 2, 0]]

        with captured_output() as out:
            board_s = tic_tac_toe.board_str(board)

        self.assertTrue(type(board_s) == str, msg='board_str should produce string representation of the board')
        self.assertTrue(len(out.getvalue()) == 0,
                        msg='board_str function shouldn\'t print anything to the standard output')

    def test_mark_board_implemented(self):
        board = [[0, 1, 0],
                 [2, 2, 1],
                 [1, 2, 0]]

        try:
            tic_tac_toe.mark_board(board, (0, 0), 1)
        except NotImplementedError:
            self.fail('The mark_board() function is either not implemented, or you forgot to delete the '
                      '\"raise NotImplemented\" expresion')

    def test_mark_board_player_1(self):
        board = [[0, 0, 0],
                 [0, 0, 0],
                 [0, 0, 0]]

        marked_board = tic_tac_toe.mark_board(board, (2, 0), 1)
        self.assertEqual(marked_board, [[0, 0, 0],
                                        [0, 0, 0],
                                        [1, 0, 0]], msg='Position (2, 0) should mark board in the lower left corner')

    def test_mark_board_player_2(self):
        board = [[0, 0, 0],
                 [0, 0, 0],
                 [0, 0, 0]]

        marked_board = tic_tac_toe.mark_board(board, (0, 1), 2)
        self.assertEqual(marked_board, [[0, 2, 0],
                                        [0, 0, 0],
                                        [0, 0, 0]],
                         msg='Position (0, 1) should mark board in the middle of the top row')

    def test_get_winner_implemented(self):
        board = [[0, 1, 0],
                 [2, 2, 1],
                 [1, 2, 0]]

        try:
            tic_tac_toe.get_winner(board)
        except NotImplementedError:
            self.fail('The get_winner() function is either not implemented, or you forgot to delete the '
                      '\"raise NotImplemented\" expresion')

    def test_get_winner_no_winner(self):
        board = [[0, 1, 0],
                 [2, 2, 1],
                 [1, 2, 0]]

        self.assertEqual(tic_tac_toe.get_winner(board), 0, msg='get_winner should return 0, if there is no winner')

    def test_get_winner_row_winner_1(self):
        board = [[0, 0, 0],
                 [1, 1, 1],
                 [2, 2, 0]]

        self.assertEqual(tic_tac_toe.get_winner(board), 1, msg='get_winner should return 1, if Player 1 marks 3 fields in one row')

    def test_get_winner_row_winner_2(self):
        board = [[1, 1, 2],
                 [1, 0, 1],
                 [2, 2, 2]]

        self.assertEqual(tic_tac_toe.get_winner(board), 2, msg='get_winner should return 2, if Player 2 marks 3 fields in one row')

    def test_get_winner_column_winner_1(self):
        board = [[1, 1, 2],
                 [1, 1, 0],
                 [2, 1, 2]]

        self.assertEqual(tic_tac_toe.get_winner(board), 1, msg='get_winner should return 1,'
                                                   ' if Player 1 marks 3 fields in one column')

    def test_get_winner_column_winner_2(self):
        board = [[1, 1, 2],
                 [1, 0, 2],
                 [2, 1, 2]]

        self.assertEqual(tic_tac_toe.get_winner(board), 2, msg='get_winner should return 2,'
                                                   ' if Player 2 marks 3 fields in one column')

    def test_get_winner_main_diagonal_winner_1(self):
        board = [[1, 1, 2],
                 [1, 1, 0],
                 [2, 1, 1]]

        self.assertEqual(tic_tac_toe.get_winner(board), 1,
                          msg='get_winner should return 1, if Player 1 marks all 3 fields on a main diagonal')

    def test_get_winner_antidiagonal_winner_1(self):
        board = [[2, 1, 1],
                 [1, 1, 0],
                 [1, 2, 2]]

        self.assertEqual(tic_tac_toe.get_winner(board), 1,
                          msg='get_winner should return 1, if Player 1 marks all 3 fields on a antidiagonal')

    def test_get_winner_main_diagonal_winner_2(self):
        board = [[2, 1, 2],
                 [1, 2, 2],
                 [0, 2, 2]]

        self.assertEqual(tic_tac_toe.get_winner(board), 2,
                          msg='get_winner should return 2, if Player 2 marks all 3 fields on a main diagonal')

    def test_get_winner_antidiagonal_winner_2(self):
        board = [[0, 1, 2],
                 [1, 2, 2],
                 [2, 2, 1]]

        self.assertEqual(tic_tac_toe.get_winner(board), 2,
                          msg='get_winner should return 2, if Player 2 marks all 3 fields on a anitdiagonal')

    def test_get_winner_with_tie(self):
        board = [[1, 1, 2],
                 [1, 2, 1],
                 [2, 2, 2]]

        self.assertEqual(tic_tac_toe.get_winner(board), 2,
                          msg='get_winner should return 2, if Player 2 wins and all fields are marked')

    def test_get_tie(self):
        board = [[1, 1, 2],
                 [2, 2, 1],
                 [1, 1, 2]]

        self.assertEqual(tic_tac_toe.get_winner(board), -1,
                          msg='get_winner should return -1, if fields are marked and there are no more moves')


