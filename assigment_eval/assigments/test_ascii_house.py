from assigment_eval import reference
from assigment_eval.tested.ascii_house import draw_house
from assigment_eval.assigments import captured_output, get_output
from itertools import product
import random
import unittest



class AsciiHouseTest(unittest.TestCase):
    longMessage = False

    def test_1_4(self):

        self.assertRaises(ValueError, lambda _: draw_house(1, 4),
                          'The function should raise ValueError if width is less then 3')

    def test_5_2(self):
        self.assertRaises(ValueError, lambda _: draw_house(5, 2),
                          'The function should raise ValueError  if height is less then 3')

    def test_4_4(self):
        self.assertRaises(ValueError, lambda _: draw_house(4, 4),
                          'The function should raise ValueError width is even number')

    def test_3_3(self):
        exp_out = " X\n" \
                  "XXX\n" \
                  "X X\n" \
                  "XXX\n"

        with captured_output() as out:
            draw_house(3, 3)

        out = get_output(out)

        self.assertTrue(out == exp_out,
                          msg=f'Incorrect output for draw_house(3, 3)\nExpected:\n{exp_out}\n\nBut got:\n{out}')

    def test_3_5(self):
        exp_out = " X\n" \
                  "XXX\n" \
                  "X X\n" \
                  "X X\n" \
                  "X X\n" \
                  "XXX\n"

        with captured_output() as out:
            draw_house(3, 5)

        out = get_output(out)


        self.assertTrue(out == exp_out,
                          msg=f'Incorrect output for draw_house(3, 5)\nExpected:\n{exp_out}\n\nBut got:\n{out}')

    def test_5_3(self):
        exp_out = "  X\n" \
                  " X X\n" \
                  "XXXXX\n" \
                  "X   X\n" \
                  "XXXXX\n"

        with captured_output() as out:
            draw_house(5, 3)

        out = get_output(out)


        self.assertTrue(out == exp_out,
                          msg=f'Incorrect output for draw_house(5, 3)\nExpected:\n{exp_out}\n\nBut got:\n{out}')

    def test_all(self):
        rand_samples = random.choices(list(product(range(3, 100, 2), range(3, 100))), k=100)
        for width, height in rand_samples:
            with captured_output() as out:
                draw_house(width, height)

            with captured_output() as exp_out:
                reference.draw_house(width, height)

            out = get_output(out)
            exp_out = get_output(exp_out)


            self.assertTrue(out == exp_out,
                              msg=f'Incorrect output for draw_house({width}, {height})\nExpected:\n{exp_out}'
                                  f'\n\nBut got:\n{out}')

