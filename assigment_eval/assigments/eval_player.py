from assigment_eval.tested.tic_tac_toe_player import Player
from assigment_eval.reference.tic_tac_toe_player import MiniMaxPlayer
from assigment_eval.tic_tac_toe.game import TicTacToe
from assigment_eval.tic_tac_toe import PlayerTimeOutException, InvalidMoveException, time_limit, TimeoutException
import pickle
from pathlib import Path

script_dir = Path(__file__).parent


class CustomUnpickler(pickle.Unpickler):
    def find_class(self, module, name):
        if name == 'MiniMaxPlayer':
            from assigment_eval.reference.tic_tac_toe_player import MiniMaxPlayer
            return MiniMaxPlayer
        return super().find_class(module, name)


def evaluate():
    with (script_dir.parent/ 'reference/players.pkl').open('rb') as f:
        opponents = CustomUnpickler(f).load()

    for opponent_name, opponent in opponents.items():
        res = {'wins': 0, 'losses': 0, 'ties': 0}
        print(f'Results against {opponent_name} player:')
        for i in range(10):
            game = TicTacToe(get_player(1), opponent[1])
            result = game.run()
            if result == 1:
                res['wins'] += 1
            elif result == 2:
                res['losses'] += 1
            else:
                res['ties'] += 1

            game = TicTacToe(opponent[0], get_player(2))
            result = game.run()
            if result == 2:
                res['wins'] += 1
            elif result == 1:
                res['losses'] += 1
            else:
                res['ties'] += 1
        print(f'\t{", ".join([f"{key}: {value}" for key, value in res.items()])}')
    exit(0)


def evaluate_handle_err():
    try:
        evaluate()
    except PlayerTimeOutException as e:
        print(e)
        exit(1)
    except InvalidMoveException as e:
        print(e)
        exit(1)
    except TimeoutException:
        print('Player timeout during initialization')
        exit(1)

def get_player(num: int):
    with time_limit(10):
        return Player(num)

evaluate_handle_err()
