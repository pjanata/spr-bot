from assigment_eval import reference
from assigment_eval.tested.fizzbuzz import fizzbuzz
from assigment_eval.assigments import captured_output

import unittest


class FizzbuzzTest(unittest.TestCase):
    def test_002(self):
        with captured_output() as (out, err):
            fizzbuzz(2)

        output = out.getvalue().strip()
        exp = '1\n2'
        self.assertEqual(output, exp,
                         msg=f'Incorrect output for fizzbuzz(2)\nExpected:\n{exp}\n\nBut got:\n{output}')

    def test_003(self):
        with captured_output() as (out, err):
            fizzbuzz(3)

        output = out.getvalue().strip()
        exp = '1\n2\nfizz'
        self.assertEqual(output, exp,
                         msg=f'Incorrect output for fizzbuzz(3)\nExpected:\n{exp}\n\nBut got:\n{output}')

    def test_005(self):
        with captured_output() as (out, err):
            fizzbuzz(5)

        output = out.getvalue().strip()
        exp = '1\n2\nfizz\n4\nbuzz'
        self.assertEqual(output, exp,
                         msg=f'Incorrect output for fizzbuzz(5)\nExpected:\n{exp}\n\nBut got:\n{output}')

    def test_015(self):
        with captured_output() as (out, _):
            fizzbuzz(15)

        with captured_output() as (exp_out, _):
            reference.fizzbuzz(15)

        output = out.getvalue().strip()
        exp = exp_out.getvalue().strip()

        self.assertEqual(output, exp,
                         msg=f'Incorrect output for fizzbuzz(15)\nExpected:\n{exp}\n\nBut got:\n{output}')

    def test_100(self):
        with captured_output() as (out, _):
            fizzbuzz(100)

        with captured_output() as (exp_out, _):
            reference.fizzbuzz(100)

        output = out.getvalue().strip()
        exp = exp_out.getvalue().strip()

        self.assertEqual(output, exp,
                         msg=f'Incorrect output for fizzbuzz(100)\n\tExpected:\n{exp}\n\nBut got:\n{output}')


