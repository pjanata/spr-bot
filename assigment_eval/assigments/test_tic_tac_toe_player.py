from assigment_eval.tested.tic_tac_toe_player import Player
import unittest
from assigment_eval.reference.tic_tac_toe_player import MiniMaxPlayer
from assigment_eval.tic_tac_toe.game import TicTacToe
from assigment_eval.tic_tac_toe import PlayerTimeOutException, InvalidMoveException
import pickle
from pathlib import Path

script_dir = Path(__file__).parent

class CustomUnpickler(pickle.Unpickler):
    def find_class(self, module, name):
        if name == 'MiniMaxPlayer':
            from assigment_eval.reference.tic_tac_toe_player import MiniMaxPlayer
            return MiniMaxPlayer
        return super().find_class(module, name)


class TicTacToePlayerTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        with (script_dir.parent/ 'reference/players.pkl').open('rb') as f:
            cls.players = CustomUnpickler(f).load()

    def test_player_number_1(self):
        player = Player(1)
        self.assertEqual(player.player_num, 1, msg='Player should have attribute player_num')

    def test_player_number_2(self):
        player = Player(2)
        self.assertEqual(player.player_num, 2, msg='Player should have attribute player_num')

    def test_against_random_1(self):
        player1 = Player(1)
        player2 = self.players['random'][1]
        game = TicTacToe(player1, player2)
        try:
            game.run()
        except PlayerTimeOutException as e:
            if e.player.player_num == 1:
                self.fail(str(e))

        except InvalidMoveException as e:
            if e.player.player_num == 1:
                self.fail(str(e))

    def test_against_random_2(self):
        player1 = self.players['random'][0]
        player2 = Player(2)
        game = TicTacToe(player1, player2)
        try:
            game.run()
        except PlayerTimeOutException as e:
            if e.player.player_num == 2:
                self.fail(str(e))

        except InvalidMoveException as e:
            if e.player.player_num == 2:
                self.fail(str(e))