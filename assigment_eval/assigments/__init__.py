import sys
from contextlib import contextmanager
from io import StringIO


@contextmanager
def captured_output():
    new_out = StringIO()
    old_out = sys.stdout
    try:
        sys.stdout = new_out
        yield sys.stdout
    finally:
        sys.stdout = old_out

def get_output(out: StringIO) -> str:
    outStr = out.getvalue().replace('\r\n', '\n')
    if outStr[-1] != '\n': outStr += '\n'
    return '\n'.join([line.rstrip() for line in outStr.split('\n')])
