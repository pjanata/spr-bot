import unittest
from assigment_eval.tested.password_check import check_password


class PasswordCheckTest(unittest.TestCase):
    def test_password_no_digit(self):
        no_digit_psw = 'nasjKJDcas!@#r'
        self.assertFalse(check_password(no_digit_psw), 'Should return False if password doesn\'t contain a digit')

    def test_password_no_lowercase(self):
        no_lower_psw = 'BADVSA!@#34R23'
        self.assertFalse(check_password(no_lower_psw), 'Should return False if password doesn\'t contain a lowercase letter')

    def test_password_no_uppercase(self):
        no_upper_psw = 'nasjcas!@#34r23'
        self.assertFalse(check_password(no_upper_psw), 'Should return False if password doesn\'t contain a uppercase letter')

    def test_password_no_special(self):
        no_special_psw = 'nasjKJDcas34r23'
        self.assertFalse(check_password(no_special_psw), 'Should return False if password doesn\'t contain a special character')

    def test_short_password(self):
        no_special_psw = 'njKJD!#23'
        self.assertFalse(check_password(no_special_psw),
                         'Should return False if the password is too short')

    def test_valid_password(self):
        valid_psw = 'nasjKJDcas!@#34r23'
        self.assertTrue(check_password(valid_psw), 'Should return True if password is valid')


if __name__ == '__main__':
    unittest.main()
